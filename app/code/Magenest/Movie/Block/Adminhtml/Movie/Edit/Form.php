<?php
namespace Magenest\Movie\Block\Adminhtml\Movie\Edit;

use Magenest\Movie\Model\ResourceModel\Director\Collection;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;

class Form extends Generic
{
    /**
     * @var Store
     */
    protected $_systemStore;

    protected $_director;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Store $systemStore
     * @param array $data
     */
    public function __construct(
        Collection $director,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Store $systemStore,
        array $data = []
    ) {
        $this->_director = $director;
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('movie_form');
        $this->setTitle(__('Movie Information'));
    }

    public function getUrlAdd()
    {
        return $this->getUrl('movie/director/new');
    }

    /**
     * Prepare form
     *
     * @return $this
     * @throws LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('movie_movie');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('movie_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Movie Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField('movie_id', 'hidden', ['name' => 'movie_id']);
        }

        $fieldset->addField(
            'movie_name',
            'text',
            ['name' => 'movie_name', 'label' => __('Movie Name'), 'title' => __('Movie Name'), 'required' => true]
        );

        $fieldset->addField(
            'description',
            'text',
            ['name' => 'description', 'label' => __('Movie Description'), 'title' => __('Movie Description'), 'required' => true]
        );

        $fieldset->addField(
            'rating',
            'text',
            ['name' => 'rating', 'label' => __('Movie Rating'), 'title' => __('Movie Rating'), 'required' => false, 'value' => '0']
        );

        $selectDirector = $this->_director->getDirectoryName();
        $fieldset->addField(
            'director_id',
            'select',
            ['name' => 'director_id',
                'label' => _('Director'),
                'title' => _('Director'),
                'required' => true,
                'values' => $selectDirector,
                'value' => '0', 'disabled' => false, 'readonly' => false,
                'after_element_html' => '<button type="button"><a href="http://intray.localhost.com/admin/movie/director/new/">Add New Director</a></button>'
            ]
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
