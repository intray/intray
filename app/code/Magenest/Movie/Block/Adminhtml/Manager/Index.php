<?php
namespace Magenest\Movie\Block\Adminhtml\Manager;

use Magento\Backend\Block\Template;
use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Customer;
use Magento\Framework\Module\FullModuleList;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\Collection;

class Index extends Template
{
    protected $_module;
    protected $_customer;
    protected $_product;
    protected $_order;
    protected $_invoice;
    protected $_creditCollection;

    public function __construct(
        Collection $creditCollection,
        Invoice $invoice,
        Order $order,
        Product $product,
        Customer $customer,
        FullModuleList $module,
        Template\Context $context,
        array $data = []
    ) {
        $this->_creditCollection = $creditCollection;
        $this->_invoice = $invoice;
        $this->_order = $order;
        $this->_product = $product;
        $this->_customer = $customer;
        $this->_module = $module;
        parent::__construct($context, $data);
    }

    public function getAllModule()
    {
        $allModule = $this->_module->getNames();
        return count($allModule);
    }

    public function getCustomModule()
    {
        $all = $this->_module->getNames();
        foreach ($all as $item) {
            $cut = explode("_", $item);
            if ($cut[0] == 'Magenest') {
                $customModule[] = $cut;
            }
        }
        return count($customModule);
    }

    public function getCustomer()
    {
        $customer = $this->_customer->getCollection()->getSize();
        return $customer;
    }

    public function getProduct()
    {
        $product = $this->_product->getCollection()->getSize();
        return $product;
    }

    public function getOrder()
    {
        $order = $this->_order->getCollection()->getSize();
        return $order;
    }

    public function getInvoice()
    {
        $invoice = $this->_invoice->getCollection()->getSize();
        return $invoice;
    }

    public function getCredit()
    {
        $credit = $this->_creditCollection->getSize();
        return $credit;
    }
}
