<?php
namespace Magenest\Movie\Block;

use Magenest\Movie\Model\Actor;
use Magenest\Movie\Model\ResourceModel\Movie\Collection;
use Magento\Framework\View\Element\Template;

class ViewMovie extends Template
{
    protected $_movieCollection;

    protected $_actor;

    public function __construct(
        Actor $actor,
        Collection $movieCollection,
        Template\Context $context,
        array $data = []
    ) {
        $this->_actor = $actor;
        $this->_movieCollection = $movieCollection;
        parent::__construct($context, $data);
    }

    public function getMovie()
    {
        $movie = $this->_movieCollection->getJoinData();
        return $movie;
    }

    public function getActor()
    {
        $actor = $this->_actor->getCollection();
        return $actor;
    }
}
