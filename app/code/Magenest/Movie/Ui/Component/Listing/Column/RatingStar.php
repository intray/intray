<?php

namespace Magenest\Movie\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class RatingStar extends Column
{
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function selectRating($star)
    {
        switch ($star) {
            case 0:
                $with = "0%";
                break;
            case 1:
                $with = "10%";
                break;
            case 2:
                $with = "20%";
                break;
            case 3:
                $with = "30%";
                break;
            case 4:
                $with = "40%";
                break;
            case 5:
                $with = "50%";
                break;
            case 6:
                $with = "60%";
                break;
            case 7:
                $with = "70%";
                break;
            case 8:
                $with = "80%";
                break;
            case 9:
                $with = "90%";
                break;
            case 10:
                $with = "100%";
                break;
        }
        return $with;
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item[$this->getData('name')] = '<div class="field-summary-rating">
                    <div class="rating-box"><div class="rating" style="width:' . $this->selectRating($item['rating']) . '"></div>
                </div>';
            }
        }
        return $dataSource;
    }
}
