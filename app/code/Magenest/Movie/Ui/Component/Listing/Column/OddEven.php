<?php
namespace Magenest\Movie\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class OddEven extends Column
{
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $orderId = $item['entity_id'];
                if ($orderId % 2 == 0) {
                    $item[$this->getData('name')] = '<div class="grid-severity-notice">Even</div>';
                } else {
                    $item[$this->getData('name')] = '<div class="grid-severity-minor">Odd</div>';
                }
            }
        }
        return $dataSource;
    }
}
