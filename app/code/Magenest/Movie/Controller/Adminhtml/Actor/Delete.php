<?php
namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Magenest\Movie\Model\Actor;
use Magento\Backend\App\Action;

class Delete extends Action
{
    protected $_model;

    public function __construct(
        Actor $model,
        Action\Context $context
    ) {
        $this->_model = $model;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Movie::actor_delete');
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_model;
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('Actor delete success'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addError(__('Actor does not exist'));
        return $resultRedirect->setPath('*/*/');
    }
}
