<?php
namespace Magenest\Movie\Controller\Adminhtml\Manager;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $resultPageFactory;

    public function __construct(
        PageFactory $resultPageFactory,
        Action\Context $context
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Movie::manager');
        $resultPage->getConfig()->getTitle()->prepend(__('Intray Manager'));
        return $resultPage;
    }
}
