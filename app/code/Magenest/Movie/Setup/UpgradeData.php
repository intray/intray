<?php
namespace Magenest\Movie\Setup;

use Magenest\Movie\Model\Actor;
use Magenest\Movie\Model\Director;
use Magenest\Movie\Model\Movie;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    protected $_actor;
    protected $_director;
    protected $_movie;

    public function __construct(
        Actor $actor,
        Director $director,
        Movie $movie
    ) {
        $this->_actor = $actor;
        $this->_director = $director;
        $this->_movie = $movie;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0.4') < 0) {
            $directors = [
                [
                    'name' => 'Director A',
                ],
                [
                    'name' => 'Director B',
                ]
            ];
            foreach ($directors as $data) {
                $director = $this->_director->setData($data)->save();
                $directorsIds[] = $director->getId();
            }

            $movies = [
                [
                    'name' => 'Doremon',
                    'description' => 'so good',
                    'rating' => '1000',
                    'director_id' => $directorsIds[0]
                ],
                [
                    'name' => 'Shin',
                    'description' => 'so good',
                    'rating' => '1500',
                    'director_id' => $directorsIds[1]
                ]
            ];

            foreach ($movies as $data) {
                $this->_movie->setData($data)->save();
            }
            $actors = [
                [
                    'name' => 'XuKa',
                ],
                [
                    'name' => 'NoBiTa',
                ],
                [
                    'name' => 'Shin SuKe',
                ],
                [
                    'name' => 'Shin FuKi',
                ],
            ];

            foreach ($actors as $data) {
                $this->_actor->setData($data)->save();
            }
        }

        $installer->endSetup();
    }
}
