<?php namespace Magenest\Movie\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        // Action to do if module version is less than 1.0.0.0
        if (version_compare($context->getVersion(), '1.0.0.1') < 0) {
            $tableName = $installer->getTable('magenest_movie');
            $tableComment = 'magenest movie';
            $columns = [
                'movie_id' => [
                    'type' => Table::TYPE_INTEGER,
                    'size' => null,
                    'options' => ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'comment' => 'movie Id',
                ],
                'name' => [
                    'type' => Table::TYPE_TEXT,
                    'size' => 255,
                    'options' => ['nullable' => false, 'default' => ''],
                    'comment' => 'Movie name',
                ],
                'description' => [
                    'type' => Table::TYPE_TEXT,
                    'size' => 2048,
                    'options' => ['nullable' => false, 'default' => ''],
                    'comment' => 'Movie description',
                ],
                'rating' => [
                    'type' => Table::TYPE_INTEGER,
                    'size' => null,
                    'options' => ['nullable' => false, 'default' => 0],
                    'comment' => 'Movie rating',
                ],
                'director_id' => [
                    'type' => Table::TYPE_INTEGER,
                    'size' => null,
                    'options' => ['unsigned' => true, 'nullable' => false],
                    'comment' => 'director id',
                ],
            ];

            $indexes =  [
                'name',
            ];

            $foreignKeys = [
                'director_id' => [
                    'ref_table' => 'magenest_director',
                    'ref_column' => 'director_id',
                    'on_delete' => Table::ACTION_CASCADE,
                ]
            ];

            /**
             *  We can use the parameters above to create our table
             */

            // Table creation
            $table = $installer->getConnection()->newTable($tableName);

            // Columns creation
            foreach ($columns as $name => $values) {
                $table->addColumn(
                    $name,
                    $values['type'],
                    $values['size'],
                    $values['options'],
                    $values['comment']
                );
            }

            // Indexes creation
            foreach ($indexes as $index) {
                $table->addIndex(
                    $installer->getIdxName($tableName, [$index]),
                    [$index]
                );
            }

            // Foreign keys creation
            foreach ($foreignKeys as $column => $foreignKey) {
                $table->addForeignKey(
                    $installer->getFkName($tableName, $column, $foreignKey['ref_table'], $foreignKey['ref_column']),
                    $column,
                    $foreignKey['ref_table'],
                    $foreignKey['ref_column'],
                    $foreignKey['on_delete']
                );
            }

            // Table comment
            $table->setComment($tableComment);

            // Execute SQL to create the table
            $installer->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '1.0.0.2') < 0) {
            $tableName = $installer->getTable('magenest_movie_actor');
            $tableComment = 'magenest movie actor';
            $columns = [
                'movie_id' => [
                    'type' => Table::TYPE_INTEGER,
                    'size' => null,
                    'options' => ['unsigned' => true, 'nullable' => false],
                    'comment' => 'movie id',
                ],
                'actor_id' => [
                    'type' => Table::TYPE_INTEGER,
                    'size' => null,
                    'options' => ['unsigned' => true, 'nullable' => false],
                    'comment' => 'actor id',
                ],
            ];

            $indexes =  [
            ];

            $foreignKeys = [
                'actor_id' => [
                    'ref_table' => 'magenest_actor',
                    'ref_column' => 'actor_id',
                    'on_delete' => Table::ACTION_CASCADE,
                ],
                'movie_id' => [
                    'ref_table' => 'magenest_movie',
                    'ref_column' => 'movie_id',
                    'on_delete' => Table::ACTION_CASCADE,
                ]
            ];

            /**
             *  We can use the parameters above to create our table
             */

            // Table creation
            $table = $installer->getConnection()->newTable($tableName);

            // Columns creation
            foreach ($columns as $name => $values) {
                $table->addColumn(
                    $name,
                    $values['type'],
                    $values['size'],
                    $values['options'],
                    $values['comment']
                );
            }

            // Indexes creation
            foreach ($indexes as $index) {
                $table->addIndex(
                    $installer->getIdxName($tableName, [$index]),
                    [$index]
                );
            }

            // Foreign keys creation
            foreach ($foreignKeys as $column => $foreignKey) {
                $table->addForeignKey(
                    $installer->getFkName($tableName, $column, $foreignKey['ref_table'], $foreignKey['ref_column']),
                    $column,
                    $foreignKey['ref_table'],
                    $foreignKey['ref_column'],
                    $foreignKey['on_delete']
                );
            }

            // Table comment
            $table->setComment($tableComment);

            // Execute SQL to create the table
            $installer->getConnection()->createTable($table);
        }

        // Changed column name table movie
        if (version_compare($context->getVersion(), '1.0.0.5', '<')) {
            $setup->getConnection()->changeColumn(
                $setup->getTable('magenest_movie'),
                'name',
                'movie_name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' => 'Movie name'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.0.6' <0)) {
            $tableName = $installer->getTable('magenest_actor');
            $fullTextIntex = ['name'];

            $setup->getConnection()->addIndex(
                $tableName,
                $installer->getIdxName($tableName, $fullTextIntex, \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT),
                $fullTextIntex,
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

        if (version_compare($context->getVersion(), '1.0.0.8') <0) {
            $tableName = $installer->getTable('magenest_director');
            $fullTextIntex = ['name'];

            $setup->getConnection()->addIndex(
                $tableName,
                $installer->getIdxName($tableName, $fullTextIntex, \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT),
                $fullTextIntex,
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }
        $installer->endSetup();
    }
}
