<?php
namespace Magenest\Movie\Model\ResourceModel\Director;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Magenest\Movie\Model\ResourceModel\Director
 */
class Collection extends AbstractCollection
{
    protected $_idFieldName = \Magenest\Movie\Model\Director::DIRECTOR_ID;
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Magenest\Movie\Model\Director', 'Magenest\Movie\Model\ResourceModel\Director');
    }

    /**
     * @return array
     */
    public function getDirectoryName()
    {
        $options[] = ['label' => '', 'value' => ''];
        foreach ($this as $key) {
            $options[] = [
                'value' => $key->getId(),
                'label' => $key->getName(),
            ];
        }
        return $options;
    }
}
