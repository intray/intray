<?php
namespace Magenest\Movie\Model\ResourceModel;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Actor extends AbstractDb
{
    protected function _construct()
    {
        // Table Name and Primary Key column
        $this->_init('magenest_actor', 'actor_id');
    }
}
