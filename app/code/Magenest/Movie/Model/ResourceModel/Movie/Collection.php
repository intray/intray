<?php
namespace Magenest\Movie\Model\ResourceModel\Movie;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = \Magenest\Movie\Model\Movie::MOVIE_ID;

    protected function _construct()
    {
        $this->_init('Magenest\Movie\Model\Movie', 'Magenest\Movie\Model\ResourceModel\Movie');
    }

    public function getJoinData()
    {
        $this->getSelect()->join(
            'magenest_director',
            'main_table.director_id = magenest_director.director_id'
        );
        return $this;
    }
}
