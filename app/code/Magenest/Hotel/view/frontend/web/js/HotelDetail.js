define(
    [
        'jquery',
        'Magento_Ui/js/modal/modal'
    ],
    function($) {
        "use strict";
        //creating jquery widget
        $.widget('Mage.modalForm', {
            options: {
                modalForm: '#modal-form',
                modalButton: '#location-city'
            },
            _create: function() {
                this.options.modalOption = this._getModalOptions();

                var modalOption = this.options.modalOption;
                var modalForm = this.options.modalForm;

                this.element.on('click', function(e){
                    var locationCity = $('#location-city').val();
                    var detailHotel = '.data-hotel';
                    $.ajax({
                        url: "hotel/index/ajax",
                        type: "POST",
                        data: {
                            locationCity: locationCity,
                        },
                        dataType: "json"
                    }).done(function (data) {
                        $(modalForm).modal(modalOption);
                        $(modalForm).trigger('openModal');
                        $('.data-hotel').html(
                            'Name: '+(data.hotel_name)+ '</br>' +
                            'Street: '+(data.location_street)+ '</br>' +
                            'City: '+(data.location_city)+ '</br>' +
                            'State: '+(data.location_state)+ '</br>' +
                            'Contact Phone: '+(data.contact_phone)+ '</br>' +
                            'Available Room: '+(data.total_available_room)
                        );
                        $(".modal-footer").hide();
                    });
                });
            },
            _getModalOptions: function() {
                var options = {
                    type: 'popup',
                    responsive: true,
                    title: 'Hotel Detail',
                };

                return options;
            },
        });

        return $.Mage.modalForm;
    }
);