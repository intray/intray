<?php
namespace Magenest\Hotel\Block\Hotel;

use Magento\Catalog\Model\Product;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
use Magenest\Hotel\Model\ResourceModel\Hotel\Collection;

class Index extends Template
{
    protected $_hotelCollectionFactory;

    protected $_product;

    protected $_registry;

    public function __construct(
        Collection $hotelCollectionFactory,
        Product $product,
        Registry $registry,
        Template\Context $context,
        array $data = []
    ) {
        $this->_hotelCollectionFactory = $hotelCollectionFactory;
        $this->_registry = $registry;
        $this->_product = $product;
        parent::__construct($context, $data);
    }

    public function getRoomType()
    {
        $product = $this->_registry->registry('current_product');
        $productId = $product->getId();
        $roomType = $this->_product->load($productId)->getData('roomType');

        return $roomType;
    }

    public function getCity()
    {
        return $this->_hotelCollectionFactory;
    }
}
