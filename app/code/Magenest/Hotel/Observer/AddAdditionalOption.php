<?php
namespace Magenest\Hotel\Observer;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\Serializer\Json;

class AddAdditionalOption implements ObserverInterface
{
    protected $serializer;

    protected $_request;

    public function __construct(
        RequestInterface $request,
        Json $serializer
    ) {
        $this->_request = $request;
        $this->serializer = $serializer;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getProduct();
        $locationCity = $this->_request->getPostValue('location');
        $additionalOptions[] = [
                'label' => "Hotel Name",
                'value' => $locationCity
            ];
        $product->addCustomOption('additional_options', $this->serializer->serialize($additionalOptions));
    }
}
