<?php
namespace Magenest\Hotel\Observer;

use Magenest\Hotel\Model\Booking;
use Magenest\Hotel\Model\Hotel;
use Magenest\Hotel\Model\ResourceModel\Hotel\CollectionFactory;
use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Cart;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;

class UpdateRoom implements ObserverInterface
{
    protected $_hotelCollectionFactory;
    protected $_product;
    protected $_hotel;
    protected $cart;
    protected $_booking;
    protected $_order;

    public function __construct(
        Order $order,
        Booking $booking,
        Product $product,
        Hotel $hotel,
        Cart $cart,
        CollectionFactory $hotelCollectionFactory
    ) {
        $this->_order = $order;
        $this->_booking = $booking;
        $this->_product = $product;
        $this->_hotel = $hotel;
        $this->cart = $cart;
        $this->_hotelCollectionFactory = $hotelCollectionFactory;
    }

    public function execute(Observer $observer)
    {
        $items = $this->cart->getQuote()->getAllVisibleItems();
        foreach ($items as $item) {
            $productId = $item->getData('product_id');
            $location = $item->getBuyRequest()->getData('location');
            $qty = $item->getQty();
            $hotel = $this->_hotel->getCollection()->addFieldToFilter('location_city', $location);
            $hotelId = $hotel->getData()['0']['hotel_id'];
            $typeRoom = $this->_product->load($productId)->getData('roomType');
            $model = $this->_hotel->load($hotelId);

            if ($typeRoom == 'single') {
                $availableSingle = $model->getData('available_single');
                $now = $availableSingle - $qty;
                $model->setData('available_single', $now);
                $model->save();
            }
            if ($typeRoom == 'triple') {
                $availableSingle = $model->getData('available_triple');
                $now = $availableSingle - $qty;
                $model->setData('available_triple', $now);
                $model->save();
            }
            if ($typeRoom == 'double') {
                $availableSingle = $model->getData('available_double');
                $now = $availableSingle - $qty;
                $model->setData('available_double', $now);
                $model->save();

                /* Save data booking */
                $quoteId = $item->getQuoteId();
                $orderStatus = $this->_order->loadByAttribute('quote_id', '127')->getStatus();
                $productName = $this->_product->load($productId)->getName();
                $createAt = $this->_product->load($productId)->getCreatedAt();

                $booking = $this->_booking;
                $booking->setData('product_name', $productName);
                $booking->setData('create_at', $createAt);
                $booking->setData('order_status', $orderStatus);
                $booking->save();
            }
        }
    }
}
