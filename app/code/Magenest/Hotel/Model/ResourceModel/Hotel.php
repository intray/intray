<?php
namespace Magenest\Hotel\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Hotel extends AbstractDb
{
    public function _construct()
    {
        $this->_init('magenest_hotel', 'hotel_id');
    }
}