<?php
namespace Magenest\Hotel\Model\ResourceModel\Booking;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = \Magenest\Hotel\Model\Booking::BOOKING_ID;

    public function _construct()
    {
        $this->_init('Magenest\Hotel\Model\Booking', 'Magenest\Hotel\Model\ResourceModel\Booking');
    }
}