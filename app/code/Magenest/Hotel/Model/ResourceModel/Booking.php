<?php
namespace Magenest\Hotel\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Booking extends AbstractDb
{
    public function _construct()
    {
        $this->_init('magenest_booking', 'booking_id');
    }
}