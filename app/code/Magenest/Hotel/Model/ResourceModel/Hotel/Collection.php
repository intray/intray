<?php
namespace Magenest\Hotel\Model\ResourceModel\Hotel;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = \Magenest\Hotel\Model\Hotel::HOTEL_ID;

    public function _construct()
    {
        $this->_init('Magenest\Hotel\Model\Hotel', 'Magenest\Hotel\Model\ResourceModel\Hotel');
    }
}
