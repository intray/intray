<?php
namespace Magenest\Hotel\Model;

use Magento\Framework\Model\AbstractModel;

class Hotel extends AbstractModel
{
    const HOTEL_ID = 'hotel_id';

    public function _construct()
    {
        $this->_init('Magenest\Hotel\Model\ResourceModel\Hotel');
    }
}
