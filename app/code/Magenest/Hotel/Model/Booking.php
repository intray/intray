<?php
namespace Magenest\Hotel\Model;

use Magento\Framework\Model\AbstractModel;

class Booking extends AbstractModel
{
    const BOOKING_ID = 'booking_id';

    public function _construct()
    {
        $this->_init('Magenest\Hotel\Model\ResourceModel\Booking');
    }
}
