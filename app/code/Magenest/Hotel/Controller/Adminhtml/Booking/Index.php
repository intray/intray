<?php
namespace Magenest\Hotel\Controller\Adminhtml\Booking;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $_resultFactory;

    public function __construct(
        PageFactory $pageFactory,
        Action\Context $context
    ) {
        $this->_resultFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->_resultFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Booking Manager'));

        return $resultPage;
    }
}
