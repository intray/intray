<?php
namespace Magenest\Hotel\Controller\Adminhtml\Hotel;

use Magenest\Hotel\Model\Hotel;
use Magento\Backend\App\Action;

class Save extends Action
{
    protected $_hotel;

    public function __construct(
        Hotel $hotel,
        Action\Context $context
    ) {
        $this->_hotel = $hotel;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $model = $this->_hotel;

        $model->setData($data);
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $model->save();
            $this->messageManager->addSuccess(__('Add Hotel Success'));
        } catch (\Exception $e) {
            $this->$this->messageManager->addError(__('Add Hotel Not Success'));
        }
        return $resultRedirect->setPath('*/*/');
    }
}
