<?php
namespace Magenest\Hotel\Controller\Adminhtml\Hotel;

use Magento\Ui\Component\MassAction\Filter;
use Magenest\Hotel\Model\ResourceModel\Hotel\CollectionFactory;
use Magento\Backend\App\Action;

class MassDelete extends Action
{
    protected $filter;

    protected $_collectionFactory;

    public function __construct(
        Filter $filter,
        CollectionFactory $collectionFactory,
        Action\Context $context
    ) {
        $this->filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $collection = $this->filter->getCollection($this->_collectionFactory->create());
        $resultRedirect = $this->resultRedirectFactory->create();
        $collectionSize = $collection->getSize();
        foreach ($collection as $item) {
            $item->delete();
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collectionSize));

        return $resultRedirect->setPath('*/*/');
    }
}
