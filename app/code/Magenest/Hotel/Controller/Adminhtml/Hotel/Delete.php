<?php
namespace Magenest\Hotel\Controller\Adminhtml\Hotel;

use Magenest\Hotel\Model\Hotel;
use Magento\Backend\App\Action;

class Delete extends Action
{
    protected $_hotel;

    public function __construct(
        Hotel $hotel,
        Action\Context $context
    ) {
        $this->_hotel = $hotel;
        parent::__construct($context);
    }

    public function execute()
    {
        $hotelId = $this->getRequest()->getParam('hotel_id');
        $model = $this->_hotel;

        $model->load($hotelId);
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $model->delete();
            $this->messageManager->addSuccess(__('Delete Hotel Success'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Delete Not Success'));
        }

        return $resultRedirect->setPath('*/*/');
    }
}
