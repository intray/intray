<?php
namespace Magenest\Hotel\Controller\Adminhtml\Hotel;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Action
{
    protected $resultPageFactory;

    public function __construct(
        PageFactory $resultPageFactory,
        Action\Context $context
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultPageFactory->create();
        $result->getConfig()->getTitle()->prepend(__('Add Hotel'));
        return $result;
    }
}
