<?php
namespace Magenest\Hotel\Controller\Index;

use Magenest\Hotel\Model\Hotel;
use Magenest\Hotel\Model\ResourceModel\Hotel\Collection;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Ajax extends Action
{
    protected $resultJsonFactory;

    protected $hotelCollection;

    protected $hotel;

    public function __construct(
        Hotel $hotel,
        Collection $hotelCollection,
        JsonFactory $resultJsonFactory,
        Context $context
    ) {
        $this->hotel = $hotel;
        $this->hotelCollection = $hotelCollection;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $locationCity = $this->getRequest()->getParam('locationCity');
        $hotel = $this->hotelCollection->addFieldToFilter('location_city', $locationCity);
        $hotelId = $hotel->getData()['0']['hotel_id'];
        $model = $this->hotel->load($hotelId);
        $data = [
          'hotel_name' => $model->getData('hotel_name'),
          'location_city' => $model->getData('location_city'),
          'location_state' => $model->getData('location_state'),
          'location_country' => $model->getData('location_country'),
          'contact_phone' => $model->getData('contact_phone'),
          'total_available_room' => $model->getData('total_available_room'),
        ];
        return $result->setData($data);
    }
}
