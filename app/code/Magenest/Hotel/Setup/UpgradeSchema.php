<?php
namespace Magenest\Hotel\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0.2') <0) {
            $tableName = $installer->getTable('magenest_booking');
            $columns = [
                'booking_id' => [
                    'type' => Table::TYPE_INTEGER,
                    'size' => null,
                    'options' => ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'comment' => 'Booking Id',
                ],
                'product_name' => [
                    'type' => Table::TYPE_TEXT,
                    'size' => 255,
                    'options' => ['nullable' => false, 'default' => ''],
                    'comment' => 'Product Name',
                ],
                'create_at' => [
                    'type' => Table::TYPE_TEXT,
                    'size' => 255,
                    'options' => ['nullable' => false, 'default' => ''],
                    'comment' => 'Create At',
                ],
                'order_status' => [
                    'type' => Table::TYPE_TEXT,
                    'size' => 20,
                    'options' => ['nullable' => false, 'default' => ''],
                    'comment' => 'Order Status',
                ],
            ];

            $indexes = [];

            $foreignKeys = [];

            $table = $installer->getConnection()->newTable($tableName);

            // Columns creation
            foreach ($columns as $name => $values) {
                $table->addColumn(
                    $name,
                    $values['type'],
                    $values['size'],
                    $values['options'],
                    $values['comment']
                );
            }

            foreach ($indexes as $index) {
                $table->addIndex(
                    $installer->getIdxName($tableName, [$index]),
                    [$index]
                );
            }

            // Foreign keys creation
            foreach ($foreignKeys as $column => $foreignKey) {
                $table->addForeignKey(
                    $installer->getFkName($tableName, $column, $foreignKey['ref_table'], $foreignKey['ref_column']),
                    $column,
                    $foreignKey['ref_table'],
                    $foreignKey['ref_column'],
                    $foreignKey['on_delete']
                );
            }

            // Execute SQL to create the table
            $installer->getConnection()->createTable($table);
        }
    }
}
