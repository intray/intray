<?php
namespace Magenest\Hotel\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('magenest_hotel');
        $columns = [
            'hotel_id' => [
                'type' => Table::TYPE_INTEGER,
                'size' => '11',
                'options' => ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'comment' => 'Hotel Id',
            ],
            'hotel_name' => [
                'type' => Table::TYPE_TEXT,
                'size' => '100',
                'options' => ['default' => ''],
                'comment' => 'Hotel Name',
            ],
            'location_street' => [
                'type' => Table::TYPE_TEXT,
                'size' => '50',
                'options' => ['default' => ''],
                'comment' => 'Hotel Location Street',
            ],
            'location_city' => [
                'type' => Table::TYPE_TEXT,
                'size' => '50',
                'options' => ['default' => ''],
                'comment' => 'Hotel Location City',
            ],
            'location_state' => [
                'type' => Table::TYPE_TEXT,
                'size' => '50',
                'options' => ['default' => ''],
                'comment' => 'Hotel Location State',
            ],
            'location_country' => [
                'type' => Table::TYPE_TEXT,
                'size' => '10',
                'options' => ['default' => ''],
                'comment' => 'Hotel Location Country',
            ],
            'contact_phone' => [
                'type' => Table::TYPE_TEXT,
                'size' => '10',
                'options' => ['default' => ''],
                'comment' => 'Hotel Contact Phone',
            ],
            'total_available_room' => [
                'type' => Table::TYPE_INTEGER,
                'size' => '2000',
                'options' => ['default' => '0'],
                'comment' => 'Hotel Available Room',
            ],
            'available_single' => [
                'type' => Table::TYPE_INTEGER,
                'size' => '100',
                'options' => ['default' => '0'],
                'comment' => 'Hotel Available Singed',
            ],
            'available_double' => [
                'type' => Table::TYPE_INTEGER,
                'size' => '100',
                'options' => ['default' => '0'],
                'comment' => 'Hotel Available Double',
            ],
            'available_triple' => [
                'type' => Table::TYPE_INTEGER,
                'size' => '1000',
                'options' => ['default' => '0'],
                'comment' => 'Hotel Available Triple',
            ],
        ];

        $indexes = [];
        $foreignKeys = [];

        $table = $installer->getConnection()->newTable($tableName);

        foreach ($columns as $name => $values) {
            $table->addColumn(
                $name,
                $values['type'],
                $values['size'],
                $values['options'],
                $values['comment']
            );
        }

        foreach ($indexes as $index) {
            $table->addIndex(
                $installer->getIdxName($tableName, [$index]),
                [$index]
            );
        }

        foreach ($foreignKeys as $column => $foreignKey) {
            $table->addForeignKey(
                $installer->getFkName($tableName, $column, $foreignKey['ref_table'], $foreignKey['ref_column']),
                $column,
                $foreignKey['ref_table'],
                $foreignKey['ref_column'],
                $foreignKey['on_delete']
            );
        }

        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
