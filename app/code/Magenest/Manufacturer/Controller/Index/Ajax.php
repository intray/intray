<?php
namespace Magenest\Manufacturer\Controller\Index;

use Magenest\Manufacturer\Model\ResourceModel\Manufacturer\Collection;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Ajax extends Action
{
    protected $resultJsonFactory;

    protected $_product;

    protected $_manufacturerCollection;

    public function __construct(
        Collection $manufacturerCollection,
        Product $product,
        JsonFactory $resultJsonFactory,
        Context $context
    ) {
        $this->_manufacturerCollection = $manufacturerCollection;
        $this->_product = $product;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $productId = $this->getRequest()->getParam('productId');
        $product = $this->_product->load($productId);
        $manufacturerName = $product->getAttributeText('manufacturer_id');
        if (!$manufacturerName) {
            $dataManufacturer = [
              'undefined' => 'Undefined'
            ];
        } else {
            $manufacturer = $this->_manufacturerCollection->addFieldToFilter('name', $manufacturerName);
            foreach ($manufacturer->getData() as $item) {
                $street = $item['address_street'];
                $city = $item['address_city'];
                $country = $item['address_country'];
                $contactName = $item['contact_name'];
                $contactPhone = $item['contact_phone'];
            }
            $dataManufacturer= [
                'name' => $manufacturerName,
                'city' => $city,
                'country' => $country,
                'street' => $street,
                'contactName' => $contactName,
                'contactPhone' => $contactPhone,
            ];
        }
        return $result->setData($dataManufacturer);
    }
}
