<?php
namespace Magenest\Manufacturer\Controller\Adminhtml\Manufacturer;

use Magenest\Manufacturer\Model\Manufacturer;
use Magento\Backend\App\Action;

class Save extends Action
{
    protected $_manufacturer;

    public function __construct(
        Manufacturer $manufacturer,
        Action\Context $context
    ) {
        $this->_manufacturer = $manufacturer;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        $model = $this->_manufacturer;
        $model->setData($data);
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $model->save();
            $this->messageManager->addSuccess(__('Save Sucessful.'));
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}
