define([
    "jquery",
    "jquery/ui"
], function($) {
    "use strict";

    //creating jquery widget
    $.widget('mage.manufacturer', {
        _create: function() {

            this.element.on('click', function(e){
                var productId = $('.it-product').val();
                $.ajax({
                    url: "manufacturer/index/ajax",
                    type: "POST",
                    data: {
                        productId: productId
                    },
                    dataType: "json"
                }).done(function (data) {
                    alert(
                        'Name: '+(data.name)+ '\n' +
                        'Street: '+(data.street)+ '\n' +
                        'City: '+(data.city)+ '\n' +
                        'Country: '+(data.country)+ '\n' +
                        'Contact Name: '+(data.contactName)+ '\n' +
                        'Contact Phone: '+(data.contactPhone)
                    );
                });
            });
        }

    });

    return $.mage.manufacturer;
});