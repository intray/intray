<?php
namespace Magenest\Manufacturer\Model;

use Magento\Framework\Model\AbstractModel;

class Manufacturer extends AbstractModel
{
    const ENTITY_ID = 'entity_id';

    protected function _construct()
    {
        $this->_init('Magenest\Manufacturer\Model\ResourceModel\Manufacturer');
    }
}
