<?php
namespace Magenest\Manufacturer\Model\ResourceModel\Manufacturer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = \Magenest\Manufacturer\Model\Manufacturer::ENTITY_ID;

    protected function _construct()
    {
        $this->_init('Magenest\Manufacturer\Model\Manufacturer', 'Magenest\Manufacturer\Model\ResourceModel\Manufacturer');
    }
}
