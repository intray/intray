<?php
namespace Magenest\Manufacturer\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Manufacturer extends AbstractDb
{
    protected function _construct()
    {
        // Table Name and Primary Key column
        $this->_init('magenest_manufacturer', 'entity_id');
    }
}
