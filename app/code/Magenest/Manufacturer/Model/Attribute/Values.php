<?php
namespace Magenest\Manufacturer\Model\Attribute;

use Magenest\Manufacturer\Model\Manufacturer;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Values extends AbstractSource
{
    protected $_manufacturer;

    public function __construct(
        Manufacturer $manufacturer
    ) {
        $this->_manufacturer = $manufacturer;
    }

    public function getAllOptions()
    {
        $manufacturer = $this->_manufacturer->getCollection();
        $options[] = ['label' => '', 'value' => ''];
        foreach ($manufacturer as $key) {
            $options[] = [
                'value' => $key->getId(),
                'label' => $key->getName(),
            ];
        }
        return $options;
    }
}
