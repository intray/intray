<?php
namespace Magenest\Manufacturer\Block\Adminhtml\Manufacturer;

use Magento\Backend\Block\Template;
use Magento\Framework\Data\Form\FormKey;
use Magenest\Manufacturer\Model\Manufacturer;

class Index extends Template
{
    protected $_manufacturer;

    protected $_formKey;

    public function __construct(
        FormKey $formKey,
        Manufacturer $manufacturer,
        Template\Context $context,
        array $data = []
    ) {
        $this->_formKey = $formKey;
        $this->_manufacturer = $manufacturer;
        parent::__construct($context, $data);
    }

    public function getManufacturer()
    {
        $manufacturer = $this->_manufacturer->getCollection()->getData();
        return $manufacturer;
    }

    public function getPostActionUrl()
    {
        return $this->getUrl('manufacturer/manufacturer/save/');
    }

    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
}
