<?php
namespace Magenest\Manufacturer\Block\Product;

use Magento\Catalog\Model\Product;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;

class Manufacturer extends Template
{
    protected $_registry;

    protected $_product;

    public function __construct(
        Product $product,
        Registry $registry,
        Template\Context $context,
        array $data = []
    ) {
        $this->_product = $product;
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }

    public function getProductId()
    {
        $product = $this->_registry->registry('current_product');
        $productId = $product->getId();
        return $productId;
    }

    public function getAttribute()
    {
        $productId = $this->getProductId();
        $attribute = $this->_product->load($productId)->getAttributeText('manufacturer_id');
        return $attribute;
    }
}
