<?php namespace Magenest\Manufacturer\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('magenest_manufacturer');
        $tableComment = 'Manufacturer Product';
        $columns = [
            'entity_id' => [
                'type' => Table::TYPE_INTEGER,
                'size' => null,
                'options' => ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'comment' => 'Manufacturer Id',
            ],
            'name' => [
                'type' => Table::TYPE_TEXT,
                'size' => 255,
                'options' => ['nullable' => false, 'default' => ''],
                'comment' => 'Manufacturer name',
            ],
            'enabled' => [
                'type' => Table::TYPE_INTEGER,
                'size' => null,
                'options' => ['unsigned' => true, 'nullable' => false],
                'comment' => 'Manufacturer Id',
            ],
            'address_street' => [
                'type' => Table::TYPE_TEXT,
                'size' => 255,
                'options' => ['nullable' => false, 'default' => ''],
                'comment' => 'Manufacturer Address Street',
            ],
            'address_city' => [
                'type' => Table::TYPE_TEXT,
                'size' => 100,
                'options' => ['nullable' => false, 'default' => ''],
                'comment' => 'Manufacturer Address City',
            ],
            'address_country' => [
                'type' => Table::TYPE_TEXT,
                'size' => 5,
                'options' => ['nullable' => false, 'default' => ''],
                'comment' => 'Manufacturer Address Country',
            ],
            'contact_name' => [
                'type' => Table::TYPE_TEXT,
                'size' => 100,
                'options' => ['nullable' => false, 'default' => ''],
                'comment' => 'Manufacturer Contact Name',
            ],
            'contact_phone' => [
                'type' => Table::TYPE_TEXT,
                'size' => 20,
                'options' => ['nullable' => false, 'default' => ''],
                'comment' => 'Manufacturer Pontact Phone',
            ],
        ];

        $indexes =  [
            // No index for this table
        ];

        $foreignKeys = [
            // No foreign keys for this table
        ];

        /**
         *  We can use the parameters above to create our table
         */

        // Table creation
        $table = $installer->getConnection()->newTable($tableName);

        // Columns creation
        foreach ($columns as $name => $values) {
            $table->addColumn(
                $name,
                $values['type'],
                $values['size'],
                $values['options'],
                $values['comment']
            );
        }

        // Indexes creation
        foreach ($indexes as $index) {
            $table->addIndex(
                $installer->getIdxName($tableName, [$index]),
                [$index]
            );
        }

        // Foreign keys creation
        foreach ($foreignKeys as $column => $foreignKey) {
            $table->addForeignKey(
                $installer->getFkName($tableName, $column, $foreignKey['ref_table'], $foreignKey['ref_column']),
                $column,
                $foreignKey['ref_table'],
                $foreignKey['ref_column'],
                $foreignKey['on_delete']
            );
        }

        // Table comment
        $table->setComment($tableComment);

        // Execute SQL to create the table
        $installer->getConnection()->createTable($table);

        // End Setup
        $installer->endSetup();
    }
}
