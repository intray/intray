<?php
namespace Magenest\Manufacturer\Setup;

use Magenest\Manufacturer\Model\Manufacturer;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    protected $_manufacturer;

    protected $eavSetupFactory;

    public function __construct(
        EavSetupFactory $eavSetupFactory,
        Manufacturer $manufacturer
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->_manufacturer = $manufacturer;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0.1') < 0) {
            $manufacturers = [
                [
                    'name' => 'Apple',
                    'enabled' => '1',
                    'address_street' => '1 Loop',
                    'address_city' => 'New York',
                    'address_country' => 'US',
                    'contact_name' => 'Mr Joel',
                    'contact_phone' => '07372822',
                ],
            ];
            foreach ($manufacturers as $data) {
                $manufacturer = $this->_manufacturer->setData($data)->save();
                $manufacturerIds[] = $manufacturer->getId();
            }
        }

        $installer->endSetup();

        if (version_compare($context->getVersion(), '1.0.0.3') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'manufacturer_id',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Manufacturer ID',
                    'input' => 'select',
                    'class' => '',
                    'source' => 'Magenest\Manufacturer\Model\Attribute\Values',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => true,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
        }
    }

}
