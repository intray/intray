define([
    'jquery',
    'Magento_Ui/js/modal/modal'
], function($, modal) {
    "use strict";

    $.widget('mage.addPopup', {
        _create: function () {
            var options = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                title: 'Login Modal',
            };

            var popup = modal(options, $('#customer-login'));
            this.element.on('click',function(){
                $("#customer-login").modal("openModal");
                $(".modal-footer").hide();
            });
        }
    });
    return $.mage.addPopup;
});