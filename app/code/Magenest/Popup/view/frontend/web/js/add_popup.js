define([
    'jquery',
    'Magento_Ui/js/modal/modal'
], function($, modal) {
    "use strict";

    $.widget('mage.addPopup', {
        _create: function () {
            var options = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                title: 'popup modal title',
                buttons: [{
                    text: $.mage.__('Close'),
                    class: '',
                    click: function () {
                        this.closeModal();
                    }
                }]
            };

            var popup = modal(options, $('#popup-modal'));
            this.element.on('click',function(){
                $("#popup-modal").modal("openModal");
            });
        }
    });
    return $.mage.addPopup;
});