define([
    "jquery",
    "mage/url",
    "mage/template",
    "jquery/ui",
    "Magento_Ui/js/modal/modal",
], function($) {
    "use strict";

    $.widget('mage.addPopup', {
        options: {
            modalForm: '#core-login',
        },
        _create: function() {
            var seft = this;
            this.options.modalOption = this._getModalOptions();
            const modalOption = this.options.modalOption;
            const modalForm = this.options.modalForm;
            var element = this.element;
            element.on('click', function(e){
                $.ajax({
                    url: "popup/index/popup",
                    type: "POST",
                    data: { },
                    dataType: "json"
                }).done(function (data) {
                    $('#core-login').html(data.html);
                    $(modalForm).modal(modalOption);
                    $(modalForm).trigger('openModal');
                    $(".modal-footer").hide();
                });
            });
        },
        _getModalOptions: function() {
            /**
             * Modal options
             */
            var options;
            options = {
                type: 'popup',
                responsive: true
            };
            return options;
        },
    });
    return $.mage.addPopup;
});
