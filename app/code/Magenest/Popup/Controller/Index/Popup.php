<?php
namespace Magenest\Popup\Controller\Index;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\LayoutFactory;

class Popup extends Action
{
    protected $_customerSession;

    protected $_jsonFactory;

    protected $_layoutFactory;

    public function __construct(
        LayoutFactory $layoutFactory,
        JsonFactory $jsonFactory,
        Session $customerSession,
        Context $context
    ) {
        $this->_layoutFactory = $layoutFactory;
        $this->_jsonFactory = $jsonFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->_jsonFactory->create();
        $customerId = $this->_customerSession->getCustomer()->getId();
        if (!$customerId) {
            $layout = $this->_layoutFactory->create();
            $layout->getUpdate()->load(['sign_in']);
            $layout->generateXml();
            $layout->generateElements();
            $html = $layout->getOutput();
            $data['html'] = $html;
            $result->setData($data);
        } else {
            $data['html'] = 'You are login';
            $result->setData($data);
        }
        return $result;
    }
}
