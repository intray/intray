<?php
namespace Magenest\Intray\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('magenest_intray_vendor');
        $tableComment = 'Magenest Customer Vendor';
        $columns = [
            'id' => [
                'type' => Table::TYPE_INTEGER,
                'size' => '11',
                'options' => ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'comment' => 'Intray Id',
            ],
            'customer_id' => [
                'type' => Table::TYPE_INTEGER,
                'size' => '11',
                'options' => ['unsigned' => true, 'nullable' => false],
                'comment' => 'Customer Id',
            ],
            'first_name' => [
                'type' => Table::TYPE_TEXT,
                'size' => '255',
                'options' => ['default' => ''],
                'comment' => 'First Name',
            ],
            'last_name' => [
                'type' => Table::TYPE_TEXT,
                'size' => '255',
                'options' => ['default' => ''],
                'comment' => 'Last Name',
            ],
            'email' => [
                'type' => Table::TYPE_TEXT,
                'size' => '255',
                'options' => ['nullable' => false, 'default' => ''],
                'comment' => 'Email',
            ],
            'company' => [
                'type' => Table::TYPE_TEXT,
                'size' => null,
                'options' => ['default' => ''],
                'comment' => 'Company',
            ],
            'phone_number' => [
                'type' => Table::TYPE_TEXT,
                'size' => '15',
                'options' => ['default' => ''],
                'comment' => 'Phone Number',
            ],
            'fax' => [
                'type' => Table::TYPE_TEXT,
                'size' => '20',
                'options' => ['default' => ''],
                'comment' => 'Fax',
            ],
            'address' => [
                'type' => Table::TYPE_TEXT,
                'size' => null,
                'options' => ['default' => ''],
                'comment' => 'Address',
            ],
            'street' => [
                'type' => Table::TYPE_TEXT,
                'size' => null,
                'options' => ['default' => ''],
                'comment' => 'Street',
            ],
            'country' => [
                'type' => Table::TYPE_TEXT,
                'size' => null,
                'options' => ['default' => ''],
                'comment' => 'Country',
            ],
            'city' => [
                'type' => Table::TYPE_TEXT,
                'size' => '50',
                'options' => ['default' => ''],
                'comment' => 'City',
            ],
            'postcode' => [
                'type' => Table::TYPE_TEXT,
                'size' => '20',
                'options' => ['default' => ''],
                'comment' => 'Postcode',
            ],
            'total_sales' => [
                'type' => Table::TYPE_FLOAT,
                'size' => '11',
                'options' => ['default' => '0'],
                'comment' => 'Total Sales',
            ],
        ];
        $indexes = [];
        $foreignKeys = [];

        $table = $installer->getConnection()->newTable($tableName);

        foreach ($columns as $name => $values) {
            $table->addColumn(
                $name,
                $values['type'],
                $values['size'],
                $values['options'],
                $values['comment']
            );
        }

        foreach ($indexes as $index) {
            $table->addIndex(
                $installer->getIdxName($tableName, [$index]),
                [$index]
            );
        }

        foreach ($foreignKeys as $column => $foreignKey) {
            $table->addForeignKey(
                $installer->getFkName($tableName, $column, $foreignKey['ref_table'], $foreignKey['ref_column']),
                $column,
                $foreignKey['ref_table'],
                $foreignKey['ref_column'],
                $foreignKey['on_delete']
            );
        }

        $table->setComment($tableComment);
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
