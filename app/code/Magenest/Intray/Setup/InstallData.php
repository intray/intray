<?php
namespace Magenest\Intray\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $_customerSetupFactory;
    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
    ) {
        $this->_customerSetupFactory = $customerSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $customerSetup = $this->_customerSetupFactory->create(['setup' => $setup]);
        $setup->startSetup();

        $customerSetup->addAttribute('customer', 'intray_is_approved', [
            'label' => 'Approved',
            'type' => 'int',
            'input' => 'boolean',
            'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'required' => false,
            'visible' => true,
            'position' => 5,
            'system' => false
        ]);

        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'intray_is_approved');
        $attribute->setData('used_in_forms', ['adminhtml_customer','customer_account_index','customer_account_create','customer_account_edit']);
        $attribute->save();
        $setup->endSetup();
    }
}