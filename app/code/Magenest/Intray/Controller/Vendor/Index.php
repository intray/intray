<?php
namespace Magenest\Intray\Controller\Vendor;

use Magenest\Intray\Model\Vendor;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Index extends Action
{
    protected $_vendor;

    public function __construct(
        Vendor $vendor,
        Context $context
    ) {
        $this->_vendor = $vendor;
        parent::__construct($context);
    }

    public function execute()
    {
        echo '<pre>';
        print_r($this->_vendor->getCollection()->getData());
        die;
    }
}
