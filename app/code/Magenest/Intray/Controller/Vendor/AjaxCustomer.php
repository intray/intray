<?php
namespace Magenest\Intray\Controller\Vendor;

use Magento\Customer\Model\Customer;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class AjaxCustomer extends Action
{
    protected $_customer;
    protected $_jsonFactory;

    public function __construct(
        Customer $customer,
        JsonFactory $jsonFactory,
        Context $context
    ) {
        $this->_customer = $customer;
        $this->_jsonFactory = $jsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->_jsonFactory->create();
        $customerId = $this->getRequest()->getParam('customerId');
        $dataCustomer = $this->_customer->load($customerId)->getData();

        if ($dataCustomer) {
            $data = [
                'email' => $dataCustomer['email'],
                'first_name' => $dataCustomer['firstname'],
                'last_name' => $dataCustomer['lastname']
            ];
            $result->setData($data);
        } else {
            $data = [
                'email' => 'Cusomer ID not null'
            ];
            $result->setData($data);
        }
        return $result;
    }
}
