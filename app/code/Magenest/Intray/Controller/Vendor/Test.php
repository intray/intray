<?php
namespace Magenest\Intray\Controller\Vendor;

use Magento\Framework\App\Action\Action;
use Magento\Directory\Model\ResourceModel\Country\Collection;
use Magento\Framework\App\Action\Context;
use Magenest\Intray\Model\ResourceModel\Vendor\Collection as Vendor;

class Test extends Action
{
    protected $_country;

    protected $_vendor;

    public function __construct(
        Vendor $_vendor,
        Collection $country,
        Context $context
    )
    {
        $this->_vendor = $_vendor;
        $this->_country = $country;
        parent::__construct($context);
    }

    public function execute()
    {
        $a = $this->_vendor->addFieldToFilter('customer_id', '1')->getData();

    }
}