<?php
namespace Magenest\Intray\Controller\Adminhtml\Vendor;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $resultPageFactory;

    public function __construct(
        PageFactory $resultPageFactory,
        Action\Context $context
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultPageFactory->create();
        $result->setActiveMenu('Magenest_Intray::vendor');
        $result->getConfig()->getTitle()->prepend(__('Manager Vendor'));
        return $result;
    }
}
