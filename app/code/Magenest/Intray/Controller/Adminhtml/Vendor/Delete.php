<?php
namespace Magenest\Intray\Controller\Adminhtml\Vendor;

use Magenest\Intray\Model\Vendor;
use Magento\Backend\App\Action;

class Delete extends Action
{
    protected $_vendor;

    public function __construct(
        Vendor $vendor,
        Action\Context $context
    ) {
        $this->_vendor = $vendor;
        parent::__construct($context);
    }

    public function execute()
    {
        $vendorId = $this->getRequest()->getParam('id');
        $model = $this->_vendor->load($vendorId);

        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $model->delete();
            $this->messageManager->addSuccess(__('Delete Vendor Success'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Delete Vendor Not Success'));
        }
        return $resultRedirect->setPath('*/*/');
    }
}
