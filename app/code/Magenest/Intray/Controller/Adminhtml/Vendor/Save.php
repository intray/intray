<?php
namespace Magenest\Intray\Controller\Adminhtml\Vendor;

use Magenest\Intray\Model\Vendor;
use Magento\Backend\App\Action;

class Save extends Action
{
    protected $_vendor;

    public function __construct(
        Vendor $vendor,
        Action\Context $context
    ) {
        $this->_vendor = $vendor;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $model = $this->_vendor;

        $model->setData($data);
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $model->save();
            $this->messageManager->addSuccess(__('Add Vendor Success'));
        } catch (\Exception $e) {
            $this->$this->messageManager->addError(__('Add Vendor Not Success'));
        }

        return $resultRedirect->setPath('*/*/');
    }
}
