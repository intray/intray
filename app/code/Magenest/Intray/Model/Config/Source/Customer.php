<?php
namespace Magenest\Intray\Model\Config\Source;

use Magento\Customer\Model\Customer as AllCustomer;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Customer extends AbstractSource
{
    protected $_customer;

    public function __construct(
        AllCustomer $customer
    ) {
        $this->_customer = $customer;
    }

    public function getAllOptions()
    {
        $options = [];
        foreach ($this->_customer->getCollection() as $key) {
            $options[] = [
                'value' => $key->getId(),
                'label' => $key->getName(),
            ];
        }
        return $options;
    }
}
