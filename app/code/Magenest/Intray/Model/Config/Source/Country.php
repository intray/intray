<?php
namespace Magenest\Intray\Model\Config\Source;

use Magento\Directory\Model\ResourceModel\Country\Collection;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Country extends AbstractSource
{
    protected $_countryCollection;

    public function __construct(
        Collection $countryCollection
    ) {
        $this->_countryCollection = $countryCollection;
    }

    public function getAllOptions()
    {
        $options = [];
        foreach ($this->_countryCollection as $item) {
            $options[] = [
                'value' => $item['iso3_code'],
                'label' => $item['iso3_code'],
            ];
        }
        return $options;
    }
}
