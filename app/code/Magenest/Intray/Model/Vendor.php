<?php
namespace Magenest\Intray\Model;

use Magento\Framework\Model\AbstractModel;

class Vendor extends AbstractModel
{
    const VENDOR_ID = 'id';

    protected function _construct()
    {
        $this->_init('Magenest\Intray\Model\ResourceModel\Vendor');
    }
}