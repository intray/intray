<?php
namespace Magenest\Intray\Model\Attribute;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Values extends AbstractSource
{
    public function getAllOptions()
    {
        $options[] = ['label' => '', 'value' => ''];
        $options[] = [
            ['label' => __('Yes'), 'value' => '2'],
            ['label' => __('No'), 'value' => '1'],
         ];
        return $options;
    }
}
