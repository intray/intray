<?php
namespace Magenest\Intray\Model\Attribute;

use Magenest\Intray\Model\ResourceModel\Vendor\Collection;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Vendor extends AbstractSource
{
    protected $_vendorCollection;

    public function __construct(
        Collection $vendorCollection
    ) {
        $this->_vendorCollection = $vendorCollection;
    }

    public function getAllOptions()
    {
        $options[] = ['label' => '', 'value' => ''];
        foreach ($this->_vendorCollection as $key) {
            $options[] = [
                'label' => $key->getLast_name(),
                'value' => $key->getCustomer_id()
            ];
        }
        return $options;
    }
}
