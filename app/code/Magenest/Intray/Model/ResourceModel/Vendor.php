<?php
namespace Magenest\Intray\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Vendor extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_intray_vendor', 'id');
    }
}