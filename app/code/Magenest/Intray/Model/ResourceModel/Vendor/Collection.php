<?php
namespace Magenest\Intray\Model\ResourceModel\Vendor;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = \Magenest\Intray\Model\Vendor::VENDOR_ID;

    protected function _construct()
    {
        $this->_init('Magenest\Intray\Model\Vendor', 'Magenest\Intray\Model\ResourceModel\Vendor');
    }
}