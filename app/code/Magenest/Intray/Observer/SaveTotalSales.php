<?php
namespace Magenest\Intray\Observer;

use Magenest\Intray\Model\Vendor;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class SaveTotalSales implements ObserverInterface
{
    protected $_vendor;

    public function __construct(
        Vendor $vendor
    ) {
        $this->_vendor = $vendor;
    }

    public function execute(Observer $observer)
    {
        $order = $observer->getData()['event']->getData()['order'];
        $customerId = $order->getCustomer_id();
        if ($customerId && $order->getStatus() == 'processing') {
            $model = $this->_vendor->load($customerId);
            $totalSales = $model->getTotal_sales();
            $model->setData('total_sales', $totalSales + $order->getTotal_qty_ordered());
            $model->save();
        }
    }
}
