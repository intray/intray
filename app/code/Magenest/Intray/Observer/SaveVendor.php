<?php
namespace Magenest\Intray\Observer;

use Magenest\Intray\Model\Vendor;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class SaveVendor implements ObserverInterface
{
    protected $_vendor;

    public function __construct(
        Vendor $vendor
    ) {
        $this->_vendor = $vendor;
    }

    public function execute(Observer $observer)
    {
        $customer = $observer->getData()['customer'];
        $data = [
            'customer_id'=>$customer->getId(),
            'email'=>$customer->getEmail(),
            'first_name'=>$customer->getFirstname(),
            'last_name'=>$customer->getLastname()
        ];

        $model = $this->_vendor;
        $model->setData($data);
        $model->save();
    }
}
