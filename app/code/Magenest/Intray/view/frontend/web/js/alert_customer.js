define([
    "jquery",
    "jquery/ui",
    "mage/url",
    "Magento_Ui/js/modal/modal",
], function ($) {
        "use strict";

        $.widget('mage.alertCustomer', {
            options: {
                modalForm: '#popup-modal',
            },
            _create: function () {
                this.options.modalOption = this._getModalOptions();
                const modalOption = this.options.modalOption;
                const modalForm = this.options.modalForm;

                this.element.on('click', function (e) {
                    var customerId = $('.customer_id').val();
                    $.ajax({
                        url: "ajaxcustomer",
                        type: "POST",
                        data: {
                            customerId: customerId
                        },
                        dataType: "json"
                    }).done(function (data) {
                        $('#popup-modal').html(
                            'Email: '+data.email +'<br>'
                            +'First Name: '+data.first_name +'<br>'
                            +'First Name: '+data.last_name
                        );
                        $(modalForm).modal(modalOption);
                        $(modalForm).trigger('openModal');
                        $(".modal-footer").hide();
                    });
                });
            },
            _getModalOptions: function() {
                /**
                 * Modal options
                 */
                var options;
                options = {
                    type: 'popup',
                    responsive: true
                };
                return options;
            },
        });

        return $.mage.alertCustomer;
});