<?php
namespace Magenest\Intray\Ui\Component\Listing\Column;

use Magento\Customer\Model\Customer;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class CustomerName extends Column
{
    protected $_customer;

    public function __construct(
        Customer $customer,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        $this->_customer = $customer;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $customerId = $item['customer_id'];
                $customerName = $this->_customer->load($customerId)->getName();
                $item[$this->getData('name')] = $customerName;
            }
        }
        return $dataSource;
    }
}
