<?php
namespace Magenest\AvatarCustomer\Block;

use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Customer\Api\CustomerRepositoryInterface;

class Avatar extends Template
{
    protected $_customerSession;

    protected $_customerInterface;

    public function __construct(
        Session $customerSession,
        Template\Context $context,
        CustomerRepositoryInterface $customerRepository,
        array $data = []
    ) {
        $this->_customerInterface = $customerRepository;
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    public function getBaseUrl()
    {
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $_objectManager->get('Magento\Store\Model\StoreManagerInterface');
        $currentStore = $storeManager->getStore();
        $baseUrl = $currentStore->getBaseUrl('media');
        return $baseUrl;
    }

    public function getAvatarCustomer()
    {
        $customerId = $this->_customerSession->getCustomer()->getId();
        $customer = $this->_customerInterface->getById($customerId);
        return $customer;
    }
}
