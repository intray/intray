<?php

namespace Magenest\AvatarCustomer\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $_customerSetupFactory;
    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
    ) {
        $this->_customerSetupFactory = $customerSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $customerSetup = $this->_customerSetupFactory->create(['setup' => $setup]);
        $setup->startSetup();

        $customerSetup->addAttribute('customer', 'avatar', [
            'label' => 'Avatar',
            'type' => 'varchar',
            'input' => 'image',
            'required' => false,
            'visible' => true,
            'position' => 5,
            'system' => false
        ]);

        $avatarAttribute = $customerSetup->getEavConfig()->getAttribute('customer', 'avatar');
        $avatarAttribute->setData('used_in_forms', ['adminhtml_customer','customer_account_index','customer_account_create','customer_account_edit']);
        $avatarAttribute->save();
        $setup->endSetup();
    }
}
